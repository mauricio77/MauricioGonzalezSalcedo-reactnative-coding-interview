import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { EmployeesStack } from './EmployeesStack';
import { UserScreen } from '../screens/User';

const Drawer = createDrawerNavigator();

export function MainNavigator() {
  return (
    <Drawer.Navigator screenOptions={{ headerShown: true }}>
      <Drawer.Screen name="DrawerEmployee" component={EmployeesStack} />
      <Drawer.Screen name="DrawerUser" component={UserScreen} />
    </Drawer.Navigator>
  );
}
