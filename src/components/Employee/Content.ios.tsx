import React from 'react';
import { Text, View } from 'react-native';
import { IEmployee } from '../../types/employee';
import styles from './styles';

interface Props {
  item: IEmployee;
}

export default (props: Props) => {
  const { item } = props;

  return (
    <View style={styles.employeeInfo}>
      <Text>
        {item.firstname} {item.lastname}
      </Text>
      <Text>{item.email}</Text>
    </View>
  );
};
