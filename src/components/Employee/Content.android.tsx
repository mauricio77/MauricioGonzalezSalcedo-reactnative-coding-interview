import React from 'react';
import { Text } from 'react-native';
import { Card } from 'react-native-paper';
import { IEmployee } from '../../types/employee';
import styles from './styles';

interface Props {
  item: IEmployee;
}

export default (props: Props) => {
  const { item } = props;

  return (
    <Card style={styles.cardContainer}>
      <Card.Title title="Card Title" subtitle="Card Subtitle" />
      <Card.Content>
        <Text>
          {item.firstname} {item.lastname}
        </Text>
        <Text>{item.email}</Text>
      </Card.Content>
    </Card>
  );
};
