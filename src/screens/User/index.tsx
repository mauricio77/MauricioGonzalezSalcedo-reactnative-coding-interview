import React from 'react';
import { Field, SafeAreaView } from '../../components';

export function UserScreen() {
  return (
    <SafeAreaView>
      <Field label="I am the user" value="Test" />
    </SafeAreaView>
  );
}
